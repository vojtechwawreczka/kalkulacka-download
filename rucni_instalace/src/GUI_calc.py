# -*- coding: utf-8 -*-
'''
Názov projektu: Kalkulačka
Súbor: GUI_calc.py
Dátum poslednej zmeny: 22.4.2017
Autori: Kristýna Zaklová (xzaklo01), Vladimír Užík (xuzik00)
Python verzia: 3.0

Popis: Implementácia grafického rozhrania kalkulačky.

'''

## @file GUI_calc.py
# @brief Implementácia grafického rozhrania kalkulačky
# @author Vladimír Užík (xuzik00)

import gi
from parser import NumericStringParser
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

## @class MainWindow
# Trieda pre hlavné okno kalkulačky.
class MainWindow:

## @var main_window
# hlavné okno

## @var nsp
# obsahuje triedu, ktorá vyhodnocuje reťazec

## @var win
# hlavné okno

## @var ent
# textové okienko

## @var old_result
# výsledok posledného výpočtu

## @var help_win
# okno s nápovedou


## @brief Metóda vytvorí hlavné okno kalkulačky, načíta súbor gui.css, ktorý definuje farby pozadia pre skupiny kláves.
    def __init__(self):

        self.nsp = NumericStringParser()
        
        builder = Gtk.Builder()
        builder.add_objects_from_file("calculator.glade", ["window1"])
        builder.connect_signals(self)
        
        self.ent = builder.get_object("entry1")

        css = Gtk.CssProvider() # css kvoli farbam
        css.load_from_path("gui.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), css,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.win = builder.get_object("window1")
        self.win.set_title("Kalkulacka")
        self.win.connect("delete-event", Gtk.main_quit)

        self.win.show_all()

        self.old_result = None

## @brief Metóda pre klávesu, ktorá pri stlačení pridá svoj názov do reťazca.
# Slúži tiež na vyresetovanie reťazca v prípade, že po zobrazení výsledku bolo zadané ďalšie číslo.
# @param button stlačená klávesa
    def on_normal_key_clicked(self, button):
        self.ent.get_style_context().remove_class("error")
        text = self.ent.get_text()
        label = button.get_label()
        if (text == str(self.old_result)) and (label in "0123456789"):
            self.ent.set_text("")
            text = ""
        replace_imm = {"xⁿ": "^", "x!": "!", "kVn": "v"}
        for k, v in replace_imm.items():
            label = label.replace(k, v)
        self.ent.set_text(text + label)

## @brief Metóda slúži na vymazanie posledného znaku reťazca zadaného do kalkulačky.
# @param button stlačená klávesa  
    def on_butBackspace_clicked(self, button):
        self.ent.get_style_context().remove_class("error")
        self.ent.set_text(self.ent.get_text()[:-1])
    
## @brief Metóda zmaže celý reťazec zadaný do kalkulačky.
# @param button stlačená klávesa
    def on_butClear_clicked(self, button):
        self.ent.get_style_context().remove_class("error")
        self.ent.set_text("")
    
## @brief Metóda slúži na vloženie posledného výsledku do reťazca.
# @param button stlačená klávesa
    def on_butAns_clicked(self, button):
        if self.old_result != None:
            self.ent.get_style_context().remove_class("error")
            self.ent.set_text(self.ent.get_text() + "Ans")

## @brief Po stlačení klávesy help metóda zobrazí nové okno s nápovedou.
# @param button stlačená klávesa   
    def on_butHelp_clicked(self, button):
        tmp = Dialog()
    
## @brief Metóda vyhodnotí reťazec po stlačení klávesy =.
# Znaky unicode nahradí za znaky, ktoré je možné použiť pri vyhodnotení matematického výrazu. V prípade, že je zadaný reťazec, ktorý nie je možné vyhodnotiť, farba pozadia sa zmení na červenú. 
# @param button stlačená klávesa
    def on_butEqu_clicked(self, button):
        text = self.ent.get_text()
        replace_imm = {"ⁿ√": "r", "π": "pi", "Ans": str(self.old_result)} 
        for k, v in replace_imm.items():
            text = text.replace(k, v)
        try:
            result = self.nsp.eval(text)
            if result == int(result):
                result = int(result)
            self.old_result = result
            self.ent.set_text(str(result))
        except:
            self.ent.get_style_context().add_class("error")

## @class Dialog
# Trieda pre okno s nápovedou.
class Dialog:

## @brief Metóda __init__ vytvára okno s nápovedou.
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file("calculator.glade", ["Help"])
        builder.connect_signals(self)
        
        self.help_win = builder.get_object("Help") # napoveda
        self.help_win.set_transient_for(main_window.win)
        self.help_win.set_title("Help")
        self.help_win.show_all()
    
## @brief Metóda pre tlačidlo 'zavrieť', ktorá slúži na zatvorenie okna s nápovedou.
    def on_butZavriet_clicked(self, button):
        self.help_win.hide()


if __name__ == "__main__":
    main_window = MainWindow()
    Gtk.main()
