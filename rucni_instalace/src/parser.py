# -*- coding: utf-8 -*-
'''
Názov projektu: Kalkulačka
Súbor: parser.py
Dátum poslednej zmeny: 22.4.2017
Autor: Vladimír Užík (xuzik00)
Python verzia: 3.0

Popis: Implementácia syntaktického analyzátoru pre prepojenie grafického prostredia a matematickej knižnice.

'''

## @file parser.py
# @brief Implementácia syntaktického analyzátoru pre prepojenie grafického prostredia a matematickej knižnice.
# @author Vladimír Užík (xuzik00)

from pyparsing import Literal, Word, Combine, Optional, ZeroOrMore, nums, oneOf, ParseException
import mathlib

##@class NumericStringParser 
#@brief Trieda slúžiaca na lexikálnu a syntaktickú analýzu reťazca zadaného do kalkulačky.
class NumericStringParser():

## @var opn
# slovník - prekladá znaky na funkcie

## @var bnf
# obsahuje celú gramatiku
 
## @var exprStack
# zásobník operandov
 
##@brief Metóda pushFirst pridá prvý token z reťazca na vrchol zásobníka operandov.
    def pushFirst(self, strg, loc, toks):
        self.exprStack.append(toks[0])

##@brief Metóda pushUMinus slúži k rozpoznaniu záporného znamienka pri čísle. 
#Ak sa vo výraze objavia dve znamienka zasebou, táto metóda zaistí, aby bolo nasledujúce číslo považované za záporné a pridá ho do zásobníka operandov.
    def pushUMinus(self, strg, loc, toks):
        if toks and toks[0] == '-':
            self.exprStack.append('unary -')

##@brief Metóda __init__ definuje gramatiku a vytvára zásobník operandov. 
#Je v nej implementovaná priorita operandov, na základe ktorej vytvára zásobník operandov.  
    def __init__(self):
        """ EBNF of calculator grammar
        point   = '.'
        factop  = '!'
        expop   = '^' | 'r' | 'v' # r -> odmocnina, v-> kVn variacie
        multop  = '*' | '/'
        addop   = '+' | '-'
        number  = '0'..'9'
        fnumber = ['+' | '-'] number+ point number*
        atom    = pi | e | fnumber
        
        fact_expr    = atom | atom factop
        exp_expr     = fact_expr [ expop fact_expr ]
        mult_expr    = exp_expr [ multop exp_expr ]
        add_expr     = add_expr [ addop add_expr ]
        """
        point = Literal(".")
        fact = Literal("!")
        pi = Literal("pi")
        e = Literal("e")
        addop = Literal("+") | Literal("-")
        multop = Literal("*") | Literal("/")
        expop = Literal("^") | Literal("r") | Literal("v")
        
        fnumber = Combine(Word(nums) + Optional(point + Optional(Word(nums))))
        atom = ((Optional("-") +
                 (e | pi | fnumber).setParseAction(self.pushFirst))).setParseAction(self.pushUMinus)
        
        fact_expr = atom + Optional((fact).setParseAction(self.pushFirst))
        exp_expr = fact_expr + ZeroOrMore((expop + fact_expr).setParseAction(self.pushFirst))
        mult_expr = exp_expr + ZeroOrMore((multop + exp_expr).setParseAction(self.pushFirst))
        add_expr = mult_expr + ZeroOrMore((addop + mult_expr).setParseAction(self.pushFirst))

        self.bnf = add_expr
        # map operator symbols to corresponding arithmetic operations
        self.opn = {"+": mathlib.add,
                    "-": mathlib.sub,
                    "*": mathlib.mul,
                    "/": mathlib.div,
                    "^": mathlib.pow,
                    "!": mathlib.factorial,
                    "r": mathlib.nthroot,
                    "v": mathlib.variation }

##@brief Metóda num konvertuje formát float na formát integer.
#@param n číslo vo formáte float
#@return Vracia číslo n vo formáte integeru.   	
    def num(self, n):
        return (float(n) if '.' in n else int(n))

##@brief Metóda evaluateStack slúži na vyhodnotenie zásobníka operandov pomocou matematickej knižnice.
#@param s zásobník operandov
#@return Vracia výsledok matematického výrazu zadaného do kalkulačky.
    def evaluateStack(self, s):
        op = s.pop()
        if op == 'unary -':
            return -self.evaluateStack(s)
        if op in "+-*/^":
            op2 = self.evaluateStack(s)
            op1 = self.evaluateStack(s)
            return self.opn[op](op1, op2)
        if op in "rv":
            op2 = self.evaluateStack(s)
            op1 = self.evaluateStack(s)
            return self.opn[op](op2, op1)
        if op == '!':
            op1 = self.evaluateStack(s)
            return self.opn[op](op1)
        if op == 'pi':
            return mathlib.pi
        if op == 'e':
            return mathlib.e
        else:
            return self.num(op)

##@brief Metóda eval vypočíta pomocou metódy evaluateStack presný výsledok reťazca zadaného do kalkulačky.
#@param parseAll=True celý reťazec spĺňa gramatiku
#@param num_string reťazec zadaný do kalkulačky 
#@return Vracia výsledok matematického výrazu zaokrúhlený na 7 desatinných
    def eval(self, num_string, parseAll=True):
        self.exprStack = []
        results = self.bnf.parseString(num_string, parseAll)
        val = self.evaluateStack(self.exprStack[:])
        return round(val, 7)

