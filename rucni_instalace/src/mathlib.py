# -*- coding: utf-8 -*-
'''
Názov projektu: Kalkulačka
Súbor: mathlib.py
Dátum poslednej zmeny: 22.4.2017
Autor: Vladimír Užík (xuzik00)
Python verzia: 3.0

Popis: Implementácia matematickej knižnice.

'''

## @file mathlib.py
# @brief Implementácia matematickej knižnice
# @author Vladimír Užík (xuzik00)

import math
import operator

## @var add 
# operátor sčítania
add = operator.add

## @var sub
# operátor odčítania
sub = operator.sub

## @var mul
# operátor násobenia
mul = operator.mul

## @var div
# operátor delenia
div = operator.truediv

## @var pi
# konštanta pí zaohkruhlená na 7 desatinných miest
pi = round(math.pi, 7)

## @var e
# Eulerovo číslo zaokrúhlené na 7 desatinných miest
e  = round(math.e, 7)


## @brief Metóda factorial slúži na výpočet faktoriálu prirodzeného čísla. 
# @param n prirodzené číslo pre výpočet faktoriálu
# @return Vracia faktoriál čísla n. V prípade chybne zadaného vstupu vyhodí výnimku. Ak je hodnota vstupu väčšia ako 10000 vyhodí ValueError
def factorial(n):
    if n > 10000: raise ValueError
    return math.factorial(n)

## @brief Metóda pow slúži na umocňovanie čísel s prirodzenými exponentmi.
# @param x základ mocniny
# @param n exponent
# @return Vracia n-tú mocninu čísla x. Ak exponent nie je prirodzené číslo, vyhodí TypeError. Ak je hodnota exponentu menšia ako 1, vyhodí ValueError.
def pow(x, n):
    try:
        if n == int(n): n = int(n) # jedine co ma napadlo aby presiel int a int-like n (12.0 == 12)
        else: raise TypeError
    except:
        raise TypeError
    if n < 1: raise ValueError
        
    return round(math.pow(x, n), 7)

## @brief Metóda nthroot slúži na výpočet všeobecnej odmocniny. 
# @param x základ odmocniny
# @param n n-tá odmocnina 
# @return Vracia n-tú odmocninu čísla x. Ak číslo n nie je prirodzené číslo, metóda vyhodí TypeError. Ak je základ odmocniny záporné a zároveň párne číslo, vyhodí ValueError.
def nthroot(x, n):
    try:
        if n == int(n): n = int(n)
        else: raise TypeError 
    except:
        raise TypeError
    if (x < 0 and n % 2 == 0) or n == 0: raise ValueError

    if x < 0 and n % 2 == 1:
        return -round(math.pow(-x, 1/float(n)),7)
    else:
        return round(math.pow(x, 1/float(n)),7)

## @brief Metóda variation slúži na výpočet variácií k-tej triedy z n-tých prvkov bez opakovania, podľa vzorca V(k,n)=n!/(n-k)!. 
# Čísla k aj n sú   prirodzené kladné čísla a zároveň číslo k <= n. 
# @param n počet prvkov
# @param k trieda
# @return Vracia počet variácií k-tej triedy z n-tých prvkov bez opakovania. Ak čísla k a n nie sú prirodzené čísla, jedná sa o TypeError. Ak sú to záporné čísla, jedná sa o ValueError.
def variation(n, k=0):

    if not isinstance(n, int) or not isinstance(k, int): raise TypeError
    if n < 0 or k < 0 or k > n: raise ValueError
        
    return factorial(n)/factorial(n-k)

